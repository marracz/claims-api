# claims-api

API to ingest claim data

## REST v1 API

* Get all claims

Path: /rest/v1/claims

* Get specified claim by id. Max length of claim id is 255 characters and should not contain whitespaces.

Path: /rest/v1/claims/{claimId}

* Get claims by insured id. Insured id should match pattern ^[A-Z]{3}[0-9]{7}$

Path: /rest/v1/claims?insuredId={insuredId}

## Configuration

Application is configured via ```application.properties``` file.

```data.path``` should indicate directory path with claim data

## Development

Testing: ```mvn clean test```

Building executable jar: ```mvn clean package```

## Build and run docker

```docker build -t claims-api target/```

```docker run -it --rm -v <host_dir_with_claims>:/claims-api/claims -p 8080:8080 claims-api``` with default ```data.path``` 