package com.decent.claimsapi.repository;

import com.decent.claimsapi.model.Claim;
import com.decent.claimsapi.rest.exception.ClaimDataFormatException;
import com.decent.claimsapi.rest.exception.DataPathException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Slf4j
public class FileClaimsRepository implements ClaimsRepository {

    private final String dataPath;
    private final ObjectMapper objectMapper;

    private static final String JSON_FILE_EXTENSION = ".json";
    private static final String FILE_NAME_PREFIX = "claim";

    public FileClaimsRepository(@Value("${data.path}") String dataPath, ObjectMapper objectMapper) {
        this.dataPath = dataPath;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void init() {
        try (Stream<Path> paths = Files.list(Paths.get(dataPath).toAbsolutePath().normalize())) {
            paths.collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Cannot read specified data directory");
            System.exit(1);
        }
    }

    @Override
    public List<Claim> getAllClaims()  {
        return readAllClaims();
    }

    @Override
    public List<Claim> getClaimsByInsuredId(String insuredId) {
        return readAllClaims().stream()
                .filter(claim -> claim.getInsured() != null)
                .filter(claim -> insuredId.equals(claim.getInsured().getInsuredId()))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Claim> getClaim(String id) {
        try {
            return readClaimFromFile(objectMapper, dataPath + FILE_NAME_PREFIX + id + JSON_FILE_EXTENSION);
        } catch (IOException e) {
            log.error("Claim data format error: {}, e", e.getMessage(), e);
            throw new ClaimDataFormatException(String.format("Claim with id %s has malformed data", id));
        }
    }

    private List<Claim> readAllClaims() {
        List<Claim> claims = new ArrayList<>();
        try (Stream<Path> paths = Files.list(Paths.get(dataPath).toAbsolutePath().normalize())) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(e -> e.toString().endsWith(JSON_FILE_EXTENSION))
                    .forEach(e -> {
                        try {
                            Optional<Claim> claim = readClaimFromFile(objectMapper, e.toString());
                            claim.ifPresent(claims::add);
                        } catch (IOException ex) {
                            log.error("Claim data format error: {}, {}", ex.getMessage(), ex);
                            throw new ClaimDataFormatException(String.format("Claim file %s has malformed data", e.getFileName()));
                        }
                    });
        } catch (IOException e) {
            log.error("Cannot read specified data directory, {}, {}", e.getMessage(), e);
            throw new DataPathException("Cannot read specified data directory");
        }
        return claims;
    }

    private Optional<Claim> readClaimFromFile(ObjectMapper mapper, String resource) throws IOException {
        String path = Paths.get(resource).toAbsolutePath().normalize().toString();
        try {
            return Optional.of(mapper.readValue(new FileReader(path), Claim.class));
        } catch (FileNotFoundException e) {
            return Optional.empty();
        }
    }
}

