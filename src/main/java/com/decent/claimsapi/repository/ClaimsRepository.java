package com.decent.claimsapi.repository;

import com.decent.claimsapi.model.Claim;

import java.util.List;
import java.util.Optional;

public interface ClaimsRepository {

    List<Claim> getAllClaims();

    List<Claim> getClaimsByInsuredId(String insuredId);

    Optional<Claim> getClaim(String id);
}
