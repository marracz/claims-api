package com.decent.claimsapi.rest.exception;

public class ClaimDataFormatException extends RuntimeException {

    public ClaimDataFormatException(String message) {
        super(message);
    }
}
