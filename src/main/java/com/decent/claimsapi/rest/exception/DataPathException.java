package com.decent.claimsapi.rest.exception;

public class DataPathException extends RuntimeException {

    public DataPathException(String message) {
        super(message);
    }
}
