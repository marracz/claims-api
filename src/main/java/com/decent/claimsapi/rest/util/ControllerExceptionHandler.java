package com.decent.claimsapi.rest.util;

import com.decent.claimsapi.rest.exception.ClaimDataFormatException;
import com.decent.claimsapi.rest.exception.ClaimNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler({ ClaimNotFoundException.class })
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDetails notFound(final Exception exception, HttpServletRequest request) {
        return createValidationErrorDetails(exception, request);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDetails badRequest(final Exception exception, HttpServletRequest request) {
        return createValidationErrorDetails(exception, request);
    }

    @ExceptionHandler({ ClaimDataFormatException.class })
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDetails internalServerError(final Exception exception, HttpServletRequest request) {
        return createValidationErrorDetails(exception, request);
    }

    private static ErrorDetails createValidationErrorDetails(Exception exception, HttpServletRequest request) {
        ErrorDetails errorDetails = new ErrorDetails();
        errorDetails.setDate(LocalDateTime.now().toString());
        errorDetails.setPath(request.getServletPath());
        if (exception.getCause() != null && exception.getCause().getMessage() != null) {
            errorDetails.setMessage(exception.getMessage() + ". " + exception.getCause().getMessage());
        } else {
            errorDetails.setMessage(exception.getMessage());
        }
        return errorDetails;
    }

}