package com.decent.claimsapi.rest;

import com.decent.claimsapi.model.Claim;
import com.decent.claimsapi.service.ClaimsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ClaimsController {

    private final ClaimsService claimsService;

    @Autowired
    public ClaimsController(ClaimsService claimsService) {
        this.claimsService = claimsService;
    }

    @ApiOperation(value = "Get all claims")
    @RequestMapping(value = "/rest/v1/claims", method = GET, produces = APPLICATION_JSON_VALUE)
    public Collection<Claim> getClaims(@ApiParam("Insured identifier") @RequestParam("insuredId") Optional<String> insuredId) {
        if (insuredId.isEmpty()) {
            return claimsService.getClaims();
        }
        return claimsService.getClaimsByInsuredId(insuredId.get());
    }

    @ApiOperation(value = "Get specified claim by id")
    @RequestMapping(value = "/rest/v1/claims/{claimId}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Claim getClaim(@ApiParam(value = "Claim identifier") @PathVariable("claimId") String id) {
        return claimsService.getClaim(id);
    }
}
