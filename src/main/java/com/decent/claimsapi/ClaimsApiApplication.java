package com.decent.claimsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClaimsApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClaimsApiApplication.class, args);
	}

}
