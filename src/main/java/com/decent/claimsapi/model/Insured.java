package com.decent.claimsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Insured {

    private String submitDate;
    private String insuredFirst;
    private String insuredLast;
    @JsonProperty("insuredID")
    private String insuredId;
}
