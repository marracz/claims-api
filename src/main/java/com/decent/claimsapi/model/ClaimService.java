package com.decent.claimsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimService {

    private int lineNumber;
    private int lineCharge;
    private String svcFromDate;
    private String svcToDate;
}
