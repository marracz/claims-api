package com.decent.claimsapi.service;

import com.decent.claimsapi.model.Claim;
import com.decent.claimsapi.repository.ClaimsRepository;
import com.decent.claimsapi.rest.exception.ClaimNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class ClaimsService {

    private final ClaimsRepository claimsRepository;

    private static final Pattern INSURED_ID_REGEXP_PATTERN = Pattern.compile("^[A-Z]{3}[0-9]{7}$");
    private static final Pattern CLAIM_ID_REGEXP_PATTERN = Pattern.compile("^\\S+$");

    @Autowired
    public ClaimsService(ClaimsRepository claimsRepository) {
        this.claimsRepository = claimsRepository;
    }

    public List<Claim> getClaims() {
        return claimsRepository.getAllClaims();
    }

    public List<Claim> getClaimsByInsuredId(String insuredId) {
        validateInsuredId(insuredId);
        return claimsRepository.getClaimsByInsuredId(insuredId);
    }

    public Claim getClaim(String id) {
        validateClaimId(id);
        return claimsRepository.getClaim(id)
                .orElseThrow(() -> new ClaimNotFoundException(String.format("Claim with id %s was not found", id)));
    }

    private void validateInsuredId(String insuredId) {
        if (!INSURED_ID_REGEXP_PATTERN.matcher(insuredId).matches()) {
            throw new IllegalArgumentException(
                    String.format("Specified insured id %s is not valid. It has to match pattern %s", insuredId, INSURED_ID_REGEXP_PATTERN.pattern())
            );
        }
    }

    private void validateClaimId(String id) {
        if (id.length() > 255 || ! CLAIM_ID_REGEXP_PATTERN.matcher(id).matches()) {
            throw new IllegalArgumentException(
                    String.format("Specified claim id %s is not valid. Max id length is 255 characters and should have no whitespaces", id)
            );
        }
    }
}
