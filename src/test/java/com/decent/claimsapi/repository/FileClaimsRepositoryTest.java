package com.decent.claimsapi.repository;

import com.decent.claimsapi.model.Claim;
import com.decent.claimsapi.rest.exception.ClaimDataFormatException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static com.decent.claimsapi.ClaimsTestHelper.readClaimFromFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class FileClaimsRepositoryTest {

    private FileClaimsRepository sutWithCorrectClaims;
    private FileClaimsRepository sutWithIncorrectClaims;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        sutWithCorrectClaims = new FileClaimsRepository("src/test/resources/correct/", objectMapper);
        sutWithIncorrectClaims = new FileClaimsRepository("src/test/resources/incorrect/", objectMapper);
    }

    @Test
    public void getClaim_fileDoesNotExist_returnEmptyOptional() {

        Optional<Claim> claim = sutWithCorrectClaims.getClaim("notexisting");
        assertTrue(claim.isEmpty());
    }

    @Test
    public void getClaim_fileExists_returnClaimFromFile() throws IOException {

        Optional<Claim> claim = sutWithCorrectClaims.getClaim("1234567");
        assertTrue(claim.isPresent());
        assertEquals(readClaimFromFile("correct/claim1234567.json"),claim.get());
    }

    @Test(expected = ClaimDataFormatException.class)
    public void getClaim_fileExistsAndContainsMalformedData_throwClaimDataFormatException() {

        sutWithIncorrectClaims.getClaim("0");
    }

    @Test
    public void getAllClaims_filesAreCorrect_returnCorrectClaims() throws IOException {

        List<Claim> expectedClaims = List.of(
                readClaimFromFile("correct/claim1234567.json"),
                readClaimFromFile("correct/claim1234568.json"),
                readClaimFromFile("correct/claim7564568.json")
        );

        List<Claim> actualClaims = sutWithCorrectClaims.getAllClaims();
        assertEquals(expectedClaims.size(), actualClaims.size());
        actualClaims.sort(Comparator.comparing(Claim::getId));
        assertEquals(expectedClaims, actualClaims);
    }

    @Test(expected = ClaimDataFormatException.class)
    public void getAllClaims_malformedClaimFile_throwClaimDataFormatException() {

        sutWithIncorrectClaims.getAllClaims();
    }

    @Test(expected = ClaimDataFormatException.class)
    public void getClaimsByInsuredId_malformedClaimFile_throwClaimDataFormatException() {

        sutWithIncorrectClaims.getClaimsByInsuredId("ABC1234567");
    }

    @Test
    public void getClaimsByInsuredId_filesAreCorrect_returnMatchingClaims() throws IOException {

        List<Claim> expectedClaims = List.of(
                readClaimFromFile("correct/claim1234568.json")
        );

        List<Claim> actualClaims = sutWithCorrectClaims.getClaimsByInsuredId("DEF1234568");
        assertEquals(expectedClaims, actualClaims);
    }

    @Test
    public void getClaimsByInsuredId_filesAreCorrectAndClaimWithGivenInsuredIdNotFound_returnEmptyCollection() {

        List<Claim> actualClaims = sutWithCorrectClaims.getClaimsByInsuredId("DEF1234560");
        assertTrue(actualClaims.isEmpty());
    }
}