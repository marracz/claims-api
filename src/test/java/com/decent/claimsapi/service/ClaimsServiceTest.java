package com.decent.claimsapi.service;

import com.decent.claimsapi.model.Claim;
import com.decent.claimsapi.repository.ClaimsRepository;
import com.decent.claimsapi.rest.exception.ClaimNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.decent.claimsapi.ClaimsTestHelper.readClaimFromFile;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClaimsServiceTest {

    private ClaimsService sut;
    @Mock
    private ClaimsRepository claimsRepository;

    @Before
    public void setUp() throws Exception {
        sut = new ClaimsService(claimsRepository);
    }

    @Test
    public void getClaims_shouldReturnAllClaimsData() throws IOException {
        List<Claim> claims = List.of(
                readClaimFromFile("correct/claim1234567.json"),
                readClaimFromFile("correct/claim1234568.json"),
                readClaimFromFile("correct/claim7564568.json")
        );

        when(claimsRepository.getAllClaims()).thenReturn(claims);

        assertEquals(claims, sut.getClaims());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaim_claimIdNotValid_shouldThrowIllegalArgumentException() {

        sut.getClaim("not valid");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaim_claimIdLongerThanValid_shouldThrowIllegalArgumentException() {

        sut.getClaim("a".repeat(256));
    }

    @Test
    public void getClaim_claimIdValidAndClaimExists_shouldReturnClaimWithGivenId() throws IOException {

        Optional<Claim> expectedClaim = Optional.of(readClaimFromFile("correct/claim1234567.json"));
        when(claimsRepository.getClaim("1234567")).thenReturn(expectedClaim);

        assertEquals(expectedClaim.get(), sut.getClaim("1234567"));
    }

    @Test(expected = ClaimNotFoundException.class)
    public void getClaim_claimIdValidAndClaimDoesNotExist_shouldThrowClaimNotFoundException() {

        when(claimsRepository.getClaim("1234567")).thenReturn(Optional.empty());

        sut.getClaim("1234567");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getClaim_insuredIdNotValid_shouldThrowIllegalArgumentException() {

        sut.getClaimsByInsuredId("invalidInsuredId");
    }

    @Test
    public void getClaimsByInsuredId_shouldReturnMatchedClaimsData() throws IOException {
        List<Claim> claims = List.of(
                readClaimFromFile("correct/claim1234567.json")
        );

        when(claimsRepository.getClaimsByInsuredId("ABC1234567")).thenReturn(claims);

        assertEquals(claims, sut.getClaimsByInsuredId("ABC1234567"));
    }
}