package com.decent.claimsapi;

import com.decent.claimsapi.model.Claim;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileReader;
import java.io.IOException;

public class ClaimsTestHelper {

    private static ObjectMapper MAPPER = new ObjectMapper();

    private static final String TEST_DATA_DIR = "src/test/resources/";

    public static Claim readClaimFromFile(String resource) throws IOException {
        return MAPPER.readValue(new FileReader(TEST_DATA_DIR + resource), Claim.class);
    }
}
